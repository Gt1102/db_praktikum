import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import schema.Item;
import schema.Review;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor

/**
 * Data Class for parsing the CsvToReview-File
 * Extract all the information and save it into the tables
 */
public class CsvToReview {

    private SessionFactory sessionFactory;
    private FileWriter logWriter;

    //Error counter
    private int errorCounterProductNotFoundReview = 0;
    private int errorCounteRatingOutOfRangeReview = 0;
    private int errorCounterDateNotViableReview = 0;
    private int getErrorCounterDoubleReview = 0;


    /**
     * Method to parse the given the CsvToReview File
     *
     * @param CsvReviewFiles List of all tje paths from the CsvToReview Files
     */
    public void parse(List<String> CsvReviewFiles) {
        List<List<String[]>> docs = new ArrayList<>();
        // Iterate through all the Files
        for (String path : CsvReviewFiles) {
            try {
                //add the List of String[], representing each CsvToReview Line to the main List
                docs.add(initDocument(path));
            } catch (IOException | URISyntaxException e) {
                System.err.println("File not found " + path);
            }
        }
        // Iterate trough each Line and saves the Review in the table
        for (List<String[]> tmp : docs) {
            saveReviews(tmp);
        }
    }

    /**
     * Method to read and extract all the information given in the CsvToReview-Document
     * @param path the path of the selected Document
     * @return a list of String[] each element of the list represent one line of the CsvToReview
     * @throws IOException if there is an input, output error
     * @throws URISyntaxException indicate that a string could not be parsed as a URI reference.
     */
    private List<String[]> initDocument(String path) throws IOException, URISyntaxException {
        List<String[]> list = new ArrayList<>();
        FileReader fr=new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        br.readLine();
        // skips the first line
        String line = br.readLine();
        while(line != null) {
            list.add(line.split("\",\""));
            line = br.readLine();
        }
        br.close();
        fr.close();

        return list;
    }

    /**
     * Method to save all the Information's extracted from the File in to the table
     * @param reviews List of all the Reviews (each Line of the CSV)
     */
    private void saveReviews(List<String[]> reviews) {

        // Iterate trough the List of CsvToReview Lines and save one item after the other
        for (String[] r : reviews) {
            ArrayList<String> errors = new ArrayList<>();
            Review review = new Review();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            // Define item by using the method getItem and get the item with the asin given from the File
            Item item = getItem(r[0].substring(1), session);

            //Check if item from the File is not defined
            if (item == null) {
                errorCounterProductNotFoundReview++;
                errors.add("product not found: " + r[0]);
            }

            review.setItem(item);
            if (Integer.parseInt(r[1]) > 5 || Integer.parseInt(r[1]) < 1) {
                errorCounteRatingOutOfRangeReview++;
                errors.add("RATING OUT OF RANGE");
            }
            review.setRating(Integer.parseInt(r[1]));

            review.setHelpful(Integer.parseInt(r[2]));
            try {
                review.setReviewdate(new SimpleDateFormat("yyyy-MM-dd").parse(r[3]));
            } catch (ParseException e) {
                errorCounterDateNotViableReview++;
                errors.add("DATE NOT VIABLE");
            }

            review.setBenutzer(r[4]);
            review.setSummary(r[5]);
            // length()-1 because of the last apostrophe (")
            review.setContent(r[6].substring(0, r[6].length() - 1));

            if (errors.isEmpty()) {
                try {
                    session.save(review);
                    session.getTransaction().commit();
                }catch (ConstraintViolationException constraintViolationException){
                    errors.add("DOUBLE REVIEW");
                    getErrorCounterDoubleReview ++;
                    try {
                        logWriter.write("Review was skipped. ERRORS: " + errors.toString() + " Review: " + review + "\n");
                    }
                    catch (IOException ioException){}
                }

            } else {
                try {
                    logWriter.write("Review was skipped. ERRORS: " + errors.toString() + " Review: " + review + "\n");
                } catch (IOException ioException) {
                }
            }
                session.close();
        }
    }

    /**
     * Method to get an item by asin from database. Returns null if not found.
     *
     * @param asin    identifier of an item
     * @param session for database
     * @return item object by asin. Returns null if not found.
     */
    private Item getItem(String asin, Session session) {
        Query query = session.createQuery("from Item where asin =:asin");
        query.setParameter("asin", asin);
        query.uniqueResult();
        Item item = (Item) query.uniqueResult();
        return item;
    }

    public String getErrorStatistic() {
        StringBuilder statistic = new StringBuilder();
        if (errorCounteRatingOutOfRangeReview != 0)
            statistic.append("Rating out of range at reviews: " + errorCounteRatingOutOfRangeReview + "\n");
        if (errorCounterDateNotViableReview != 0)
            statistic.append("Date not valid at reviews: " + errorCounterDateNotViableReview + "\n");
        if (getErrorCounterDoubleReview != 0)
            statistic.append("Double Reviews: " + getErrorCounterDoubleReview + "\n");
        if (errorCounterProductNotFoundReview != 0)
            statistic.append("Product not found at reviews: " + errorCounterProductNotFoundReview + "\n");
        return statistic.toString();
    }

}

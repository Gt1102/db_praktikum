import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import schema.Categorie;
import schema.Item;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor

/**
 * Class to manage parsing of xml-category files
 */
public class XMLToCategory {

    private SessionFactory sessionFactory;
    private FileWriter logWriter;

    private int errorCounterAsinNotFound = 0;

    /**
     * manage parse procedure of category files.
     * creates list of documents
     * iterate throw documents and saves as category
     *
     * @param xmlCategoriePaths needs list of paths from xml-category-documents
     */
    public void parse(List<String> xmlCategoriePaths) {
        List<Document> xmlCategorieFiles = new ArrayList<>();
        for (String path : xmlCategoriePaths) {
            xmlCategorieFiles.add(initDocument(path));
        }
        for (Document doc : xmlCategorieFiles) {
            saveAllCategories(doc);
        }
    }

    /**
     * methode to generate xml-category-document from path
     * @param path of category.xml document
     * @return xml-category-document
     */
    private Document initDocument(String path) {
        File xmlFile = new File(path);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            return documentBuilder.parse(xmlFile);
        } catch (IOException | SAXException | ParserConfigurationException exception) {
            System.exit(-1);
            return null;
        }
    }

    /**
     * saves all categories in database. Starts recursive method
     *
     * @param doc xml-document
     */
    private void saveAllCategories(Document doc) {
        Node parent = doc.getDocumentElement();
        NodeList childs = parent.getChildNodes();

        // iterate over all subcategories from parent node
        for (int i = 0; i < childs.getLength(); i++) {
            if (childs.item(i).getNodeType() == 1)
                // saves categories in database
                saveCategorie(childs.item(i), true);
        }
    }

    /**
     * recursive method to save a category an there subcategories
     * @param node it is an categorie in xml-format
     * @return a saved categorie object.
     */
    private Categorie saveCategorie(Node node, Boolean main) {

        // create new category object
        Categorie categorie = new Categorie();
        if (main) categorie.setMainCategorie(true);
        // name categorie and delete parse leftover (line space)
        categorie.setName(node.getFirstChild().getNodeValue().replace("\n", ""));

        NodeList childs = node.getChildNodes();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        // check if category has subcategories or items
        if (childs.getLength() != 0) {
            //iterate over all subcategories
            for (int i = 0; i < childs.getLength(); i++) {
                // if node is subcategorie call method recursively with child node and add the return as subcategorie
                if (childs.item(i).getNodeName().equals("category")) {
                    categorie.getSubcategories().add(saveCategorie(childs.item(i), false));
                }
                // if node is an item
                if (childs.item(i).getNodeName().equals("item")) {
                    // get asin of item
                    String asin = childs.item(i).getFirstChild().getNodeValue();
                    // get item object from database
                    Item item = getItem(asin, session);
                    if (item != null) {
                        // add item
                        categorie.getItems().add(item);
                    } else {
                        try {
                            errorCounterAsinNotFound++;
                            logWriter.write("cant find item with asin: " + asin + "to put it in category: " + categorie + "\n");
                        } catch (IOException ioException) {
                        }
                    }
                }
            }
        }
        try {
            session.saveOrUpdate(categorie);
            session.getTransaction().commit();
        } catch (ConstraintViolationException persistenceException) {
        }

        session.close();
        return categorie;
    }


    /**
     * Method to get an item by asin from database. Returns null if not found.
     *
     * @param asin    identifier of an item
     * @param session for database
     * @return item object by asin. Returns null if not found.
     */
    private Item getItem(String asin, Session session) {
        Query query = session.createQuery("from Item where asin =:asin");
        query.setParameter("asin", asin);
        query.uniqueResult();
        Item item = (Item) query.uniqueResult();
        return item;
    }

    public String getErrorStatistic(){
        StringBuilder statistic = new StringBuilder();
        if (errorCounterAsinNotFound != 0)statistic.append("Items not found to save in category: ").append(errorCounterAsinNotFound).append("\n");
        return statistic.toString();
    }

}

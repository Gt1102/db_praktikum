package schema;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@ToString
@Getter
@Setter
@Entity
@NoArgsConstructor
/**
 * Class for storing the reviews in the database
 */
public class Review {
    /**
     * The identifier of the Review
     */
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Review is in a ManyToOne Relationship with Item, A Review is made for one specific Item, but a Item could have many different review from different users
     * <Review>--m--has--1--<Item>
     */
    @ManyToOne
    private Item item;

    private int rating;

    private int helpful;

    private Date reviewdate;

    private String benutzer;

    /**
     * Extend the size of the attribute content
     */
    @Column(columnDefinition = "TEXT")
    private String content;


    private String summary;
}

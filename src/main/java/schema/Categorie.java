package schema;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
@Entity
@ToString

/**
 * Class for storing categories in the Database
 */

public class Categorie {
    @Id
    @ToString.Exclude
    @GeneratedValue()
    private long id;

    private String name;

    /**
     * Set of all subcategories in a separate table
     * <Categorie>--1--is--m--<Categorie>
     */
    @OneToMany
    @ToString.Exclude
    private Set<Categorie> subcategories = new HashSet();

    /**
     * Set of all items from book, which are in the categorie, stored in a separate table
     * <Item>--m--is--m--<Item>
     */
    @ManyToMany
    @ToString.Exclude
    private Set<Item> items = new HashSet<>();

    private Boolean mainCategorie = false;
}

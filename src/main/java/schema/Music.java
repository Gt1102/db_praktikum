package schema;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Entity
@Setter
@Getter
/**
 * Class for storing the entity Music in the Database
 * Music is a child of items and extends his attributes
 */
public class Music extends Item {

    /**
     * Constructor of Music
     * @param element the element of the xml which contains the attributes of Music
     * @param errorWriter
     */
    public Music(Element element, FileWriter errorWriter) {
        super(element, errorWriter);

        artist = getPersons(element, "artist");
        creators = getPersons(element, "creator");
        labels = getPersons(element, "label");
        listmania = getPersons(element, "list");
        tracks = getPersons(element, "title");

        binding = element.getElementsByTagName("binding").item(0).getTextContent();
        format = element.getElementsByTagName("format").item(0).getTextContent();

        try {
            numDisc = Integer.parseInt(element.getElementsByTagName("num_discs").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            numDisc = -1;
        }
        try {
            upc = Long.parseLong(element.getElementsByTagName("upc").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            upc = -1;
        }
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(element.getElementsByTagName("releasedate").item(0).getTextContent());
        } catch (ParseException parseException) {
        }
    }


    /**
     * Method to extract all the involved Persons
     * @param element the xml element to get all the Persons
     * @param person Role of the Person
     * @return List of all people which fits the criteria role
     */
    private List<String> getPersons(Element element, String person) {
        List<String> persons = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(person);
        for (int i = 0; i < nodeList.getLength(); i++) {
            persons.add(nodeList.item(i).getTextContent().replace("\n", ""));
        }
        return persons;
    }

    /**
     * List of all listed items in listmania in a separate table
     */
    @ElementCollection()
    private List<String> listmania = new ArrayList<>();

    /**
     * List of all artist from Music in a separate table
     */
    @ElementCollection()
    private List<String> artist = new ArrayList<>();

    /**
     * List of all artist from Music in a separate table
     */
    @ElementCollection()
    private List<String> creators = new ArrayList<>();

    /**
     * List of all labels from Music in a separate table
     */
    @ElementCollection()
    private List<String> labels=new ArrayList<>();

    /**
     * List of all listed tracks from Music in a separate table
     */
    @ElementCollection()
    private List<String> tracks=new ArrayList<>();

    // Musicspecific attributes
    private String binding;

    private String format;

    private int numDisc;

    private Date date;

    private long upc;

}

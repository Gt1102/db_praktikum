package schema;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Entity
@Setter
@Getter
/**
 * Class for storing Books in the Database
 * Book is a child of items and extends his attributes
 */
public class Book extends Item {

    /**
     * Constructor of Book
     * @param element the element of the xml which contains the attributes of a Book
     * @param errorWriter Filewriter which writes all the errors in the errorlog
     */
    public Book(Element element, FileWriter errorWriter) {
        super(element, errorWriter);

        authors = getPersons(element, "author");
        publishers = getPersons(element, "publishers");
        listmania = getPersons(element, "list");

        binding = element.getElementsByTagName("binding").item(0).getTextContent();

        Element el = (Element) element.getElementsByTagName("edition").item(0);
        edition = el.getAttribute("val");

        el = (Element) element.getElementsByTagName("isbn").item(0);
        if (!el.getAttribute("val").equals("")) isbn = el.getAttribute("val");

        el = (Element) element.getElementsByTagName("package").item(0);

        try {
            packageHeight = Integer.parseInt(el.getAttribute("height"));
        } catch (NumberFormatException numberFormatException) {
            numberFormatException.getCause();
        }
        try {
            packageWeight = Integer.parseInt(el.getAttribute("weight"));
        } catch (NumberFormatException numberFormatException) {
        }
        try {
            packageLength = Integer.parseInt(el.getAttribute("length"));
        } catch (NumberFormatException numberFormatException) {
        }
        try {
            pages = Integer.parseInt(element.getElementsByTagName("pages").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            pages = -1;
        }
        el = (Element) element.getElementsByTagName("publication").item(0);
        try {
            publicationDate = new SimpleDateFormat("yyyy-MM-dd").parse(el.getAttribute("date"));
        } catch (ParseException numberFormatException) {
        }

    }


    /**
     * Method to extract all the involved Persons
     * @param element the xml element to get all the Persons
     * @param person Role of the Person
     * @return List of all people which fits the criteria role
     */
    private List<String> getPersons(Element element, String person) {
        List<String> persons = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(person);
        for (int i = 0; i < nodeList.getLength(); i++) {
            persons.add(nodeList.item(i).getTextContent().replace("\n", ""));
        }
        return persons;
    }

    /**
     * List of all authors from book in a seperate table
     */
    @ElementCollection()
    private List<String> authors = new ArrayList<>();

    /**
     * List of all publishers from book in a separate table
     */
    @ElementCollection()
    private List<String> publishers = new ArrayList<>();

    /**
     * List of all listed items in listmania in a separate table
     */
    @ElementCollection()
    private List<String> listmania = new ArrayList<>();


    // BookSpecific attributes

    private String edition;

    private String isbn;

    private Date publicationDate;

    private int pages;

    private String binding;

    private int packageLength;

    private int packageHeight;

    private int packageWeight;




}

package schema;


import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
/**
 * Class for storing shops in the database
 */
public class Shop {

    /**
     * The identifier of shop
     */
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String street;

    @NotNull
    private String zip;

}

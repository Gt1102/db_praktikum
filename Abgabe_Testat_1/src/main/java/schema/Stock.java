package schema;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
/**
 * Class for storing Stocks in the Database
 * Stock is use to represent the specific orders of each item depending on the shop
 */
public class Stock {

    /**
     * The Identifier of Stocl
     */
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     private long id;

    /**
     * Stock is in a ManyToOne Relationship with Shop, A shop has a lot of different stock, but each specific stock belongs to one shop
     * <Stock>--m--has--1--<Shop>
     */
    @ManyToOne
    private Shop shop;

    /**
     * Stock is in a ManyToOne Relationship with Item, A Item has a lot of different stock depending on the shop, but each stock is set for one Item
     * <Stock>--m--has--1--<Item>
     */
    @ManyToOne
    private Item item;

    private String state;

    private double price;

    private float mult;

    private String currency;
}

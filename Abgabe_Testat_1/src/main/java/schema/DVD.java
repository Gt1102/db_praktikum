package schema;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@NoArgsConstructor
@Entity
@Setter
@Getter
/**
 * Class for storing DVD´s in the Database
 * DVD is a child of items and extends his attributes
 */
public class DVD extends Item {

    /**
     * Constructor of DVD
     * @param element the element of the xml which contains the attributes of a DVD
     * @param errorWriter Filewriter which writes all the errors in the errorlog
     */
    public DVD(Element element, FileWriter errorWriter) {
        super(element, errorWriter);

        actors = getPersons(element, "actor");
        creators = getPersons(element, "creator");
        directors = getPersons(element, "director");
        studios = getPersons(element, "studio");

        aspectratio = element.getElementsByTagName("aspectratio").item(0).getTextContent();
        format = element.getElementsByTagName("format").item(0).getTextContent();
        try {
            regionCode = Integer.parseInt(element.getElementsByTagName("regioncode").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            regionCode = -1;
        }
        try {
            runtime = Integer.parseInt(element.getElementsByTagName("runningtime").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            //runtime = -1;
        }
        try {
            theatrRelease = Integer.parseInt(element.getElementsByTagName("theatr_release").item(0).getTextContent());
        } catch (NumberFormatException numberFormatException) {
            theatrRelease = -1;
        }
        Element el = (Element) element.getElementsByTagName("upc").item(0);
        try {
            upcValue = Long.parseLong(el.getAttribute("val"));
        } catch (NumberFormatException numberFormatException) {
            upcValue = -1;
        }
        try {
            releasedate = new SimpleDateFormat("yyyy-MM-dd").parse(element.getElementsByTagName("releasedate").item(0).getTextContent());
        } catch (ParseException e) {
        }

    }

    /**
     * Method to extract all the involved Persons
     * @param element the xml element to get all the Persons
     * @param person Role of the Person
     * @return List of all people which fits the criteria role
     */
    private List<String> getPersons(Element element, String person) {
        List<String> persons = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(person);
        for (int i = 0; i < nodeList.getLength(); i++) {
            persons.add(nodeList.item(i).getTextContent().replace("\n", ""));
        }
        return persons;
    }

    /**
     * List of all studios from DVD in a separate table
     */
    @ElementCollection
    private List<String> studios = new ArrayList<>();

    /**
     * List of all actors from DVD in a separate table
     */
    @ElementCollection
    private List<String> actors = new ArrayList<>();

    /**
     * List of all directors from DVD in a separate table
     */
    @ElementCollection
    private List<String> directors = new ArrayList<>();

    /**
     * List of all creators from DVD in a seperate table
     */
    @ElementCollection
    private List<String> creators = new ArrayList<>();

    // DVDSpecific attributes


    private String aspectratio;

    private String format;

    private Date releasedate;

    private int runtime;

    private long upcValue;

    private int regionCode;

    private int theatrRelease;

}

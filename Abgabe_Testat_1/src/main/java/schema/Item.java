package schema;

import com.sun.istack.NotNull;
import lombok.*;
import org.w3c.dom.Element;

import javax.persistence.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
/**
 * Using this strategy, each class in the hierarchy is mapped to its table
 * The only column which repeatably appears in all tables is the identifier
 */
@Inheritance(strategy = InheritanceType.JOINED)

/**
 * Class for Storing the main attributes (asin, ean, salerank, title) each item has
 * Is the parent class of book, dvd and music
 */
public class Item {

    @Transient
    private ArrayList<String> errors = new ArrayList();


    /**
     * Constructor of Item
     *
     * @param element     the element of the xml which contains the attributes of Item
     * @param errorWriter Filewriter which writes all the errors in the errorlog
     */
    public Item(Element element, FileWriter errorWriter) {
        setItemAttribute(element, errorWriter);
    }

    /**
     * parse attributes from xml node to item object
     * @param element xml-item
     * @param errorWriter errorLogWriter to write errors in file
     */
    private void setItemAttribute(Element element, FileWriter errorWriter) {
        asin = element.getAttribute("asin");
        if (asin.equals("")) {
            errors.add("NO ASIN FOUND");
        }
        try {
            salerank = (Integer.parseInt(element.getAttribute("salesrank")));
        } catch (NumberFormatException e) {
        }

        Element el =(Element) element.getElementsByTagName("details").item(0);
        if (el != null) img = el.getAttribute("img");

        try {
            title = element.getElementsByTagName("title").item(0).getTextContent();
        } catch (NullPointerException nullPointerException) {
            try {
                errorWriter.write(element.getAttribute("asin") + " was skipped, because because no title is available." + System.lineSeparator());
            } catch (IOException ioException) {
            } finally {
                errors.add("NO TITLE FOUND");
            }
        }
        try {
            ean = element.getElementsByTagName("ean").item(0).getTextContent();
        } catch (NullPointerException nullPointerException) {
            ean = element.getAttribute("ean");
        }
    }


    /**
     * The identifier of Item
     */
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String asin;

    @NotNull
    private String title;

    private int salerank;

    private String ean;

    private String img;

    /**
     * Set of all items with their similar Item
     * The attribute similar is an ManyToMany Relationship with itself
     * <Item>--m--is--n--<Item>
     */
    @ToString.Exclude
    @ManyToMany
    private Set<Item> similars = new HashSet<>();


}

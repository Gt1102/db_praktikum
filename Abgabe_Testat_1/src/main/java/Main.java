import org.hibernate.cfg.Configuration;
import org.xml.sax.SAXException;
import schema.*;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class. Controls program flow
 */
public class Main {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        // prepare configuration
        Configuration configuration = new Configuration();
        configuration.configure();
        // add classes
        configuration.addAnnotatedClass(Item.class);
        configuration.addAnnotatedClass(DVD.class);
        configuration.addAnnotatedClass(Book.class);
        configuration.addAnnotatedClass(Music.class);
        configuration.addAnnotatedClass(Stock.class);
        configuration.addAnnotatedClass(Shop.class);
        configuration.addAnnotatedClass(Categorie.class);
        configuration.addAnnotatedClass(Review.class);

        try {
            // prepare errorLog file
            File errorLog = new File("errorLog.txt");
            FileWriter logWriter = new FileWriter("errorLog.txt");

            // prepare xmlToItem class to parse xml to item
            XMLToItem xmlToItem = new XMLToItem(configuration.buildSessionFactory(), logWriter, 0, 0, 0, 0, 0);
            // prepare XMLToCategory class to parse xml to categories
            XMLToCategory xmlToCategory = new XMLToCategory(configuration.buildSessionFactory(), logWriter,0);
            // prepare CsvToReview class to parse csv to reviews
            CsvToReview csvToReview = new CsvToReview(configuration.buildSessionFactory(), logWriter, 0, 0, 0);

            // create list of xml-item-files and add paths
            List<String> xmlItemFiles = new ArrayList<>();
            xmlItemFiles.add("data_dresden.xml");
            xmlItemFiles.add("data_leipzig_transformed.xml");

            // parse prepared xml-item-file list and save in database
            xmlToItem.parseDocument(xmlItemFiles);

            // create list of xml-category-files and add paths
            List<String> xmlCategorieFiles = new ArrayList<>();
            xmlCategorieFiles.add("data_categories.xml");

            // parse prepared xml-category-file list and save in database
            xmlToCategory.parse(xmlCategorieFiles);

            // create list of csv-review-files and add paths
            List<String> csvReviewFiles = new ArrayList<>();
            csvReviewFiles.add("data_reviews.csv");

            // parse prepared csv-review-file list and save in database
            csvToReview.parse(csvReviewFiles);

            // write statistics in log
            logWriter.write(xmlToItem.getErrorStatistic() + csvToReview.getErrorStatistic() + xmlToCategory.getErrorStatistic());


            logWriter.close();

        } catch (IOException ioException){
            ioException.getStackTrace();
        }
    }
}
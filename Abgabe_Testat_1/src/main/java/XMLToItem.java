import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import schema.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
/**
 * Data Class for parsing the XML-File
 * Extract all the information and save it into the tables
 */
class XMLToItem {


    private final SessionFactory sessionFactory;
    private final FileWriter errorWriter;

    private int errorCounterNoAsin = 0, errorCounterNoTitle = 0, errorCounterWrongPgroup = 0, errorCounterNoPrice = 0, errorCounterItemAlreadyInDB;

    /**
     * Method to parse all the given XML Files
     *
     * @param paths List of all the paths from the XML File
     */
    public void parseDocument(List<String> paths) {
        List<Document> documents = new ArrayList<>();
        // Iterate trough all the Files
        for (String path : paths) {
            try {
                // Extract the informations from the File
                Document document = initDocument(path);
                documents.add(document);
                // Create shop
                Shop shop = initShop(document);
                // Extract all the product from the specific shop and save them in to a table
                saveAllItems(document, shop);
            } catch (IOException | SAXException | ParserConfigurationException e) {
                e.printStackTrace();
            }
        }
        // Set all the similar Items
        for (Document document : documents) {
            setSimilars(document);
        }

    }

    /**
     * Method to read and extract all the information given in the XML-Document
     * @param path the path of the selected Document
     * @return the parsed Document
     * @throws IOException if there is an input, output error
     * @throws SAXException Encapsulate a general SAX error or warning.
     * @throws ParserConfigurationException Indicates a serious configuration error.
     */
    private Document initDocument(String path) throws IOException, SAXException, ParserConfigurationException {
        File xmlFile = new File(path);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        return documentBuilder.parse(xmlFile);
    }

    /**
     * method to set connection between similar items. Saves set of similar items in an item object
     * @param document needs an xml-document of items
     */
    private void setSimilars(Document document) {
        NodeList children = document.getDocumentElement().getChildNodes();
        // iterat over all child nodes
        for (int i = 0; i < children.getLength(); i++) {
            Node nNode = children.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nNode;
                String asin = element.getAttribute("asin");

                Session session = sessionFactory.openSession();
                session.beginTransaction();
                // get item by asin form database
                Item item = getItem(asin, session);

                Element similars = (Element) element.getElementsByTagName("similars").item(0);
                NodeList items = similars.getElementsByTagName("item");
                if (items.getLength() == 0) {
                    items = similars.getElementsByTagName("asin");
                }
                if (item != null) {
                    // iterate over all similar items
                    for (int j = 0; j < items.getLength(); j++) {
                        Element itemElement = (Element) items.item(j);
                        //get asin from similar itemNode
                        String similarAsin = itemElement.getAttribute("asin").equals("") ? itemElement.getTextContent() : itemElement.getAttribute("asin");
                        // get item by asin from database
                        Item similarItem = getItem(similarAsin, session);
                        if (!item.getSimilars().contains(similarItem)) {
                            item.getSimilars().add(similarItem);
                            session.update(item);
                            session.flush();
                        }
                    }

                }
                session.close();
            }
        }
    }


    /**
     * Method to get the Shop of the XML and set all of this attributes
     * @param document the XML File
     * @return the extracted Shop with their attributes
     */
    public Shop initShop(Document document) {
        Session session = sessionFactory.openSession();
        Shop shop = new Shop();
        ArrayList<String> errors = new ArrayList<>();
        if (document.getDocumentElement().getAttribute("name").equals("")) errors.add("NO NAME");
        else shop.setName(document.getDocumentElement().getAttribute("name"));
        if (document.getDocumentElement().getAttribute("street").equals("")) errors.add("NO STREET");
        else shop.setStreet(document.getDocumentElement().getAttribute("street"));
        if (document.getDocumentElement().getAttribute("zip").equals("")) errors.add("NO ZIP");
        else shop.setZip(document.getDocumentElement().getAttribute("zip"));
        try {
            session.beginTransaction();
            session.save(shop);
            session.close();
        } catch (ConstraintViolationException constraintViolationException) {
            try {
            errorWriter.write("errors: " + errors + " " + shop + "\n");
            } catch (IOException ioException){}
        }
        return shop;
    }

    /**
     * Method to save all the products/Items given in the Document
     * @param document The Xml File
     * @param shop To identify which items are in that specific shop, to know which one to save in which shop
     * @throws IOException if there is an input, output error
     */
    public void saveAllItems(Document document, Shop shop) throws IOException {
        NodeList children = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node nNode = children.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nNode;

                Session session = sessionFactory.openSession();
                session.beginTransaction();
                Item item = getItem(element.getAttribute("asin"), session);


                // lege neues Item an, wenn Item noch nicht in Datenbank vorhanden
                if (item == null) {
                    switch (element.getAttribute("pgroup").toLowerCase()) {
                        case "music":
                            item = new Music(element, errorWriter);
                            addErrors(item);
                            if (!item.getErrors().isEmpty()) {
                                errorWriter.write(item + "\n");
                            } else {
                                session.save(item);
                                session.flush();
                            }
                            break;
                        case "dvd":
                            item = new DVD(element, errorWriter);
                            addErrors(item);
                            if (!item.getErrors().isEmpty()) {
                                errorWriter.write(item + "\n");
                            } else {
                                session.save(item);
                                session.flush();
                            }
                            break;
                        case "book":
                            item = new Book(element, errorWriter);
                            addErrors(item);
                            if (!item.getErrors().isEmpty()) {
                                errorWriter.write(item + "\n");
                            } else {
                                session.save(item);
                                session.flush();
                            }
                            break;
                        default:
                            errorCounterWrongPgroup++;
                            errorWriter.write(element.getAttribute("asin") + " was skipped, because '" + element.getAttribute("pgroup") + "' is not an valid product." + "\n");
                            break;
                    }
                }
//                else {
//                    errorCounterItemAlreadyInDB ++;
//                    errorWriter.write("Item is already in the database: " + item + "\n");
//                }

                // speichere item in stock, wenn es nicht null ist und keine Fehler aufweist
                if (item != null && item.getErrors().isEmpty()) {
                    prepareStock(element, shop, item, session);
                }
                session.close();
                // speichere

            }
        }
    }


    /**
     * Method to get an item by asin from database. Returns null if not found.
     *
     * @param asin    identifier of an item
     * @param session for database
     * @return item object by asin. Returns null if not found.
     */
    private Item getItem(String asin, Session session) {
        Query query = session.createQuery("from Item where asin =:asin");
        query.setParameter("asin", asin);
        query.uniqueResult();
        return (Item) query.uniqueResult();
    }


    /**
     * Method to set the Stock of the Items for the specific Shop and save the stock in the database
     *
     * @param element the element of the xml which contains the attributes of Stock
     * @param shop    the specific Shop to identify how to set the stocks
     * @param item    the selected item which is supposed to get notify in the shop
     * @param session Session to save the item and the stock
     * @return the stock of the item or return null if item not available
     */
    private Stock prepareStock(Element element, Shop shop, Item item, Session session) {
        Stock stock = new Stock();
        Element el = (Element) element.getElementsByTagName("price").item(0);
        stock.setShop(shop);

        stock.setState(el.getAttribute("state"));
        stock.setMult(Float.parseFloat(el.getAttribute("mult")));
        stock.setCurrency(el.getAttribute("currency"));
        stock.setItem(item);
        try {
            stock.setPrice(Double.parseDouble(el.getTextContent()));
            // wenn eine Item kein Preis besitzt, dann ist es in diesem Shop nicht verfügbar und somit wird es nicht in den Bestand des Shops aufgenummen und nicht gespeichert. -> return null
        } catch (NumberFormatException numberFormatException) {
            //try {
            //errorCounterNoPrice++;
            //errorWriter.write("The item was rejected, no price: " + stock + "\n");
            //} catch (IOException ioException) {
            //}
            return null;
        }
        if (!session.isOpen()) {
            session = sessionFactory.openSession();
            session.beginTransaction();
        }
        try {
            if (stock.getItem().getErrors().isEmpty())
                session.save(stock);

        } catch (Exception e) {
            e.printStackTrace();
        }
        session.close();

        return stock;
    }

    /**
     * Method to count errors from Item
     *
     * @param item
     */
    private void addErrors(Item item) {
        for (String error : item.getErrors()) {
            if (error.equals("NO ASIN FOUND")) errorCounterNoAsin++;
            else if (error.equals("NO TITLE FOUND")) errorCounterNoTitle++;
        }
    }

    /**
     * Method to get error statistics
     *
     * @return
     */
    public String getErrorStatistic() {
        StringBuilder statistic = new StringBuilder();
        if (errorCounterNoAsin != 0)
            statistic.append("Items without asin: ").append(errorCounterNoAsin).append("\n");
        if (errorCounterNoTitle != 0)
            statistic.append("Items without title: ").append(errorCounterNoTitle).append("\n");
        if (errorCounterWrongPgroup != 0)
            statistic.append("Entries with wrong Pgroup: ").append(errorCounterWrongPgroup).append("\n");
        if (errorCounterNoPrice != 0)
            statistic.append("Items without price: ").append(errorCounterNoPrice).append("\n");
        if (errorCounterItemAlreadyInDB != 0)
            statistic.append("Items already included in database: ").append(errorCounterItemAlreadyInDB).append("\n");
        return statistic.toString();
    }
}
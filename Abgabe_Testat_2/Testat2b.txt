
ALTER TABLE item
    ADD averagerating DOUBLE PRECISION CHECK (averagerating >= 1 AND averagerating <= 5);


CREATE FUNCTION update_average_rating() RETURNS trigger
    LANGUAGE plpgsql as
$$
BEGIN
    UPDATE ITEM SET averagerating = (SELECT AVG(rating) FROM review WHERE item.id = review.item_id);
    RETURN NEW;
END;
$$;


CREATE TRIGGER updateAvg
    AFTER INSERT OR UPDATE OR DELETE
    ON review
EXECUTE PROCEDURE update_average_rating();


-- Ein Beispiel um den Trigger zu testen
INSERT INTO review (benutzer, content, helpful, rating, reviewdate, summary, item_id)
VALUES ('asdf', 'asdf', 4, 3, now(), 'asvycv', 1);

